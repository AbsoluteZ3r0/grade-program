/*
 * grade.cxx
 * 
 * Copyright 2014 Alex <alex@Yoga-foshizzle>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>	//for string compare, etc
#include <fstream>	//'ofstream': stream class to write on files; 'ifstream': stream class to read from files; 'fstream': stream class to both read and write from/to files.
//#include <sys/stat.h>
using namespace std;

int inputSemester(string yearLetter);
double inputGrades(double partOFtotal);
void makeNewFile(char fileNameArr[]);
bool checkIFthere(string fileName);
int fileNameErrorCheck(string fileName);	//as the string is being checked, make it a char array and then send to makeNewFile, as it needs to be a char array to make it a file 


int main(int argc, char **argv){
	char yearSeason[7];	//example: 2014F (2014 Fall)		//implement summer semester later
	cout << "What semester are you looking for? ";
	cin >> yearSeason;
	inputSemester(yearSeason);
	cout << "What class are you going to put your grades in for? ";
	return 0;
	
}

int inputSemester(string yearLetter){
//	ifstream filein;						//input file stream
	string input;
	cout << endl << "Got it." << endl;
	cout << yearLetter << endl;
	int check = 0;
	if(checkIFthere(yearLetter) == 0){	//file is not there and needs to be made
		cout << "File not found...creating one." << endl;
		while(check != 1){
			cout << "That didn't fit the criteria." << endl;
			cout << "The criteria needs to be: " << endl;
			cout << "sp (for spring) and f (for fall) and then the year (four digits)" << endl;
			cout << "Please input file name: ";
			cin >> input;
			cout << endl;
			check = fileNameErrorCheck(input);
		}
		/*
		 * Send this to a function that error checks the users input
		 * then, if that's ok, send it to makeNewFile(yearLetter);
		 */
	}
	else if(checkIFthere(yearLetter) == 1){	//file is there, open for reading/appending
		cout << "File found...reading or appending." << endl;
		
	}
	return 0;
}

double inputGrades(double partOFtotal){
	int counter = 0;							// to keep track of where in the array the grades are
	double grade;
	double topGrade;							// Used for the fraction where you need to find the percent
	double bottomGrade;							// Used for the fraction where you need to find the percent
	double arrayOFgrades[30];
	int keepGettingGrades = 1; 					//it will keep asking for grade input untill it is false or '0'
	char percORfrac[10];						//If the grade is in a percentage or a fraction
	printf("\n When you are done, press 0 when asked for if percent or fraction, to stop inserting.");
	
	while(keepGettingGrades == 1)
		{
		printf("\n Is this in a percentage or a fraction? (perc or frac)\n");
		scanf("%s", percORfrac);
		
		if(strncmp(percORfrac, "perc", 4) == 0){
			printf("\n Please insert number:  ");
			scanf("%lf", &grade);
			arrayOFgrades[counter] = grade;
		
		}
		else if(strncmp(percORfrac, "frac", 4) == 0){
			printf("\n Please insert the top number:  ");
			scanf("%lf", &topGrade);
			printf("\n Please insert the bottom number: ");
			scanf("%lf", &bottomGrade);
			grade = ((topGrade/bottomGrade) * 100);
			arrayOFgrades[counter] = grade;
		}
		else if(strncmp(percORfrac, "0", 1) == 0){
			keepGettingGrades = 0;
		}

		counter++;
	}
	
	
	// Just some more debugging stuff, don't mind the mess
	int c = 0;
	
	while(c < counter){
		printf("Grade in spot zero %lf", arrayOFgrades[c]);
		c++;
	}
	return 0;
};
//TODO: do error checking on filename input 
int fileNameErrorCheck(string fileName){
	char fileNameArray[fileName.size()+1];
	int length;
	memcpy(fileNameArray,fileName.c_str(),fileName.size());
	int check = 0;
	if(fileNameArray[0] == 'f'){	//for fall semester
		if(fileNameArray[1] == '2'){
			if(fileNameArray[2] == '0'){
				length = strlen(fileNameArray);
				if(length < 6){
					makeNewFile(fileNameArray);
					check++;
					return check;
				}
			}
		}
	}
	if(fileNameArray[0] == 's'){
		if(fileNameArray[1] == 'p'){
			if(fileNameArray[2] == '2'){
				if(fileNameArray[3] == '0'){
					length = strlen(fileNameArray);
					if(length < 7){
						makeNewFile(fileNameArray);
						check++;
						return check;
					}
				}
			}
		}
	}
	return check;
}

void makeNewFile(char fileNameArr[]){
	// MAKE THIS A CHAR AFTER RUNNING THROUGH AN ERROR CHECK FUNCTION
//	char fileN[10];
	FILE* fp = fopen(fileNameArr, "w");
	fprintf(fp, "TEST!");
	fclose(fp);
	cout << "File make successful." << endl;
}

bool checkIFthere(string fileName){		//checking if the file has already been made or not
	ifstream checkFile (fileName.c_str());
	if(checkFile.is_open())
		return true;
	else
		return false;
}
